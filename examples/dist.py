from mpi4py import MPI
from pymumps import Mumps
import scipy.sparse as spp
import numpy as np

# Initialize mumps wrapper
driver_mumps = Mumps('d')
# Get corresponding numpy type
nptype = driver_mumps.nptype

# MPI stuff
comm = MPI.COMM_WORLD
rank = comm.Get_rank()
nprocs = comm.Get_size()

# Set fortran communicator
id = driver_mumps.id
id.comm_fortran = comm.py2f()

# Host is involved in parallel steps
id.par = 1

# Init mumps
driver_mumps.initialize()
id.sym = 0

# Set global matrix A
n = 15

grow = np.array([0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6,
                 6, 6, 7, 7, 7, 7,  7, 8, 8, 8,  8, 9, 9,  9,  9, 10, 10, 10, 10, 10, 11, 11, 11, 11, 12, 12, 12, 13, 13, 13, 13, 14, 14, 14])
gcol = np.array([0, 1, 3, 0, 1, 2, 4, 1, 2, 5, 0, 3, 4, 6, 1, 3, 4, 5, 7, 2, 4, 5, 8, 3, 6, 7,
                 9, 4, 6, 7, 8, 10, 5, 7, 8, 11, 6, 9, 10, 12,  7,  9, 10, 11, 13,  8, 10, 11, 14,  9, 12, 13, 10, 12, 13, 14, 11, 13, 14])
gdata = np.zeros(grow.shape, dtype='f8')

for k, (i, j) in enumerate(zip(grow, gcol)):
    if i == j:
        gdata[k] = 3.0
    else:
        gdata[k] = -1.0

glob_A = spp.coo_matrix((gdata, (grow, gcol)), shape=(n, n))

# Compute RHS from an exepected solution
sol_th = np.arange(n)
rhs = glob_A @ sol_th

# Now compute local matrix for proc 0 and 1
# NB: no interface needed (Mumps sums values with same indices)

row = []
col = []
data = []

for i, j, v in zip(grow, gcol, gdata):
    if rank == 0 and i <= 8:
        row.append(i)
        col.append(j)
        data.append(v)
    if rank == 1 and i >= 9:
        row.append(i)
        col.append(j)
        data.append(v)

A = spp.coo_matrix((data, (row, col)), shape=(n, n))

# Set icntl
if rank == 0:
    driver_mumps.ICNTL[1] = 6
    driver_mumps.ICNTL[2] = 6
    driver_mumps.ICNTL[3] = 6
    driver_mumps.ICNTL[4] = 6

# Distributed input
driver_mumps.ICNTL[5] = 0
driver_mumps.ICNTL[18] = 3

driver_mumps.set_A(A)

# Rhs is centralized
if rank == 0:
    driver_mumps.set_RHS(rhs)

# Call mumps
driver_mumps.drive(6)

# Get centralized solution
if rank == 0:
    sol = driver_mumps.get_solution()
    print("Found:")
    print(sol)
    print("Expected:")
    print(sol_th)

# Finalize mumps
driver_mumps.finalize()

if rank == 0:
    print("done")
