from setuptools import setup, find_packages
setup(
    name="pymumps",
    version="0.1",
    description="a wrapper to call the Mumps solver",
    url="https://gitlab.inria.fr/gmarait/PYMUMPS",
    author="Gilles Marait",
    author_email="gilles.marait@inria.fr",
    license="CeCILL-C",
    packages=find_packages(),
)
