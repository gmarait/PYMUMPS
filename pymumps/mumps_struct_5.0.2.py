#
# Python wrapper for mumps
# 03/11/16
# Tested on mumps version 5.0.2
# Author: Gilles Marait
#

from ctypes import *

class Mumps_struct():

    MUMPS_VERSION_MAX_LEN = 25

    ###
    # Mumps scalar data types

    class cmumps_complex(Structure):
        _fields_ = [("r", c_float),
                    ("i", c_float)]

    dmumps_complex = c_double
    smumps_complex = c_float

    class zmumps_complex(Structure):
        _fields_ = [("r", c_double),
                    ("i", c_double)]

    # Create the mumps structures

    class cmumps_struct(Structure):
        pass

    class dmumps_struct(Structure):
        pass

    class smumps_struct(Structure):
        pass

    class zmumps_struct(Structure):
        pass

    #
    ###

    def __init__(self, precision):
        precs = ['c', 'd', 's', 'z']
        if precision not in precs:
            raise ValueError("Precision given is " + str(precision) + ". Not in " + str(precs))

        self.precision = precision

    def get_struct_type(self):
        try:
            Mumps_struct.cmumps_struct._fields_ = self.__get_fields('c')
            Mumps_struct.dmumps_struct._fields_ = self.__get_fields('d')
            Mumps_struct.smumps_struct._fields_ = self.__get_fields('s')
            Mumps_struct.zmumps_struct._fields_ = self.__get_fields('z')
        except AttributeError:
            # Ignore error if the values are already set
            pass
        struct_dict = {'c' : Mumps_struct.cmumps_struct,
                       'd' : Mumps_struct.dmumps_struct,
                       's' : Mumps_struct.smumps_struct,
                       'z' : Mumps_struct.zmumps_struct}
        return struct_dict[self.precision]

    def get_scal_type(self):
        scal_dict = {'c' : Mumps_struct.cmumps_complex,
                     'd' : Mumps_struct.dmumps_complex,
                     's' : Mumps_struct.smumps_complex,
                     'z' : Mumps_struct.zmumps_complex}
        return scal_dict[self.precision]

    def get_real_type(self):
        real_dict = {'c' : c_float,
                     'd' : c_double,
                     's' : c_float,
                     'z' : c_double}
        return real_dict[self.precision]

    def get_numpy_type(self):
        np_dict = {'c' : 'c8',
                   'd' : 'f8',
                   's' : 'f4',
                   'z' : 'c16'}

        return np_dict[self.precision]

    def __get_fields(self, precision):
        """
        Create the structure fields depending on the scalar type
        """
        scal = self.get_scal_type()
        real = self.get_real_type()

        return  [("sym", c_int),
                 ("par", c_int),
                 ("job", c_int),
                 ("comm_fortran", c_int),
                 ("icntl", c_int * 40),
                 ("keep", c_int * 500),
                 ("cntl", real * 15),
                 ("dkeep", real * 130),
                 ("keep8", c_int64 * 150),
                 ("n", c_int),
                 ("nz_alloc", c_int),
                 ("nz", c_int),
                 ("irn", POINTER(c_int)),
                 ("jcn", POINTER(c_int)),
                 ("a", POINTER(scal)),
                 ("nz_loc", c_int),
                 ("irn_loc", POINTER(c_int)),
                 ("jcn_loc", POINTER(c_int)),
                 ("a_loc", POINTER(scal)),
                 ("nelt", c_int),
                 ("eltptr", POINTER(c_int)),
                 ("eltvar", POINTER(c_int)),
                 ("a_elt", POINTER(scal)),
                 ("perm_in", POINTER(c_int)),
                 ("sym_perm", POINTER(c_int)),
                 ("uns_perm", POINTER(c_int)),
                 ("colsca", POINTER(real)),
                 ("rowsca", POINTER(real)),
                 ("colsca_from_mumps", c_int),
                 ("rowsca_from_mumps", c_int),
                 ("rhs",        POINTER(scal)),
                 ("redrhs",     POINTER(scal)),
                 ("rhs_sparse", POINTER(scal)),
                 ("sol_loc",    POINTER(scal)),
                 ("irhs_sparse", POINTER(c_int)),
                 ("irhs_ptr",    POINTER(c_int)),
                 ("isol_loc",    POINTER(c_int)),
                 ("nrhs",       c_int),
                 ("lrhs",       c_int),
                 ("lredrhs",    c_int),
                 ("nz_rhs",     c_int),
                 ("lsol_loc",   c_int),
                 ("schur_mloc", c_int),
                 ("schur_nloc", c_int),
                 ("schur_lld",  c_int),
                 ("mblock", c_int),
                 ("nblock", c_int),
                 ("nprow", c_int),
                 ("npcol", c_int),
                 ("info", c_int * 40),
                 ("infog", c_int * 40),
                 ("rinfo", real * 40),
                 ("rinfog", real * 40),
                 ("deficiency", c_int),
                 ("pivnul_list", POINTER(c_int)),
                 ("mapping", POINTER(c_int)),
                 ("size_schur", c_int),
                 ("listvar_schur", POINTER(c_int)),
                 ("schur", POINTER(scal)),
                 ("instance_number", c_int),
                 ("wk_user", POINTER(scal)),
                 ("version_number", c_char * (Mumps_struct.MUMPS_VERSION_MAX_LEN + 2)),
                 ("ooc_tmpdir", c_char * 256),
                 ("ooc_prefix", c_char * 64),
                 ("write_problem", c_char * 256),
                 ("lwk_user", c_int)]
