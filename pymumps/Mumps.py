#
# Python wrapper for mumps
# 03/11/16
# Tested on mumps version 5.2.1
# Author: Gilles Marait
#

from ctypes import *
from ctypes.util import find_library
import numpy as np
import scipy.sparse as spp
import os

from .mumps_struct import *

dir_path = os.path.dirname(os.path.realpath(__file__))

class MumpsError(Exception):
    """ Exception for mumps error"""

    def __init__(self, message):
        self.message = message

class Mumps():

    def __init__(self, precision):
        """
        Initialize the mumps wrapper with a given scalar type and precision
        precision: one of 'c','d','s','z' (ignore case)
        """

        # Check precision / scalar type
        self.precision = precision.lower()
        av_precisions = ['c', 'd', 's', 'z']
        if self.precision not in av_precisions:
            raise ValueError("Mumps precision must be 'c','d','s' or 'z'")

        self.is_complex = (self.precision == 'c' or self.precision == 'z')

        self.libmumps = self.__load_mumps()

        struct = Mumps_struct(self.precision)

        # Create the mumps structure
        self.__struct_type = struct.get_struct_type()
        self.__scal_type = struct.get_scal_type()
        self.__real_type = struct.get_real_type()
        self.nptype = struct.get_numpy_type()

        self.id = self.__struct_type()
        self.__id_ptr = pointer(self.id)

        # Set intern values
        self.__is_init = False

        # Get some permanent pointers for the data
        self.__rows = None
        self.__cols = None
        self.__data = None
        self.__rhs = None
        self.__listvar = None
        self.__schur = None
        self.__schur_f = None

        # CNTLS and INFOS with fortran numbering
        self.ICNTL = {}
        self.CNTL = {}
        self.INFO = {}
        self.RINFO = {}
        self.INFOG = {}
        self.RINFOG = {}

        # Set driver function
        if self.precision == 'c':
            self.__driver = self.libmumps.cmumps_c
        elif self.precision == 'd':
            self.__driver = self.libmumps.dmumps_c
        elif self.precision == 's':
            self.__driver = self.libmumps.smumps_c
        elif self.precision == 'z':
            self.__driver = self.libmumps.zmumps_c

        self.__driver.argtypes = [POINTER(self.__struct_type)]

        # Initialize
        self.initialize()

    def __update_cntls(self):
        """
        Update CNTLS (assumes fortran numbering in the dictionaries)
        """
        for i, v in self.ICNTL.items():
            self.id.icntl[i-1] = c_int(v)
        for i, v in self.CNTL.items():
            self.id.cntl[i-1] = self.__real_type(v)

    def __update_infos(self):
        """
        Update INFOS (assumes fortran numbering in the dictionaries)
        """
        for i in range(40):
            self.INFO[i+1] = self.id.info[i]
            self.RINFO[i+1] = self.id.rinfo[i]
            self.INFOG[i+1] = self.id.infog[i]
            self.RINFOG[i+1] = self.id.rinfog[i]

    def __load_mumps(self):
        """
        Load the mumps library
        Only support linux systems at the moment
        """

        # The path should be loaded in the libcache
        # See __init__.py
        from .libcache import libs_cache
        libname = self.precision + 'mumps'
        lib = libs_cache[libname]
        return cdll.LoadLibrary(lib)

    def check_err(self):

        err_info = self.id.info[0]
        err_info2 = self.id.info[1]

        if err_info < 0:
            message = "Mumps error: INFO(1) = " + str(err_info) + "\n"
            message += "             INFO(2) = " + str(err_info2)
            raise MumpsError(message)

    def initialize(self):
        """
        Initialize mumps (job = -1)
        NB: 'comm', 'sym' and 'par' must have been set in the structure
        """
        self.drive(-1)
        self.__is_init = True

        # Check that version is 5.2.1
        version = self.id.version_number
        if version != b"5.2.1":
            raise EnvironmentError('This wrapper is make for version 5.2.1 of MUMPS. Found version: ' + str(version))

    def finalize(self):
        """
        Finalize mumps (job = -2)
        """
        if not self.__is_init:
            raise RuntimeError('Try to finalize mumps when mumps is not initialized')
        self.id.job = -2
        self.drive()
        self.__is_init = False

    def drive(self, id_job=None):
        """
        Check data type and call mumps driver
        The job id must have been set if job_id is not given
        """
        self.__update_cntls()

        if id_job:
            self.id.job = id_job
        else:
            id_job = self.id.job

        self.__driver(self.__id_ptr)

        if id_job > 0:
            self.__update_infos()

        self.check_err()

    def set_A(self, A):
        """
        Assume A can be cast into a scipy.sparse.coo_matrix
        """
        self.__update_cntls()
        A = spp.coo_matrix(A, dtype=self.nptype)
        n = A.shape[0]
        nz = A.getnnz()

        # Fortran numbering
        self.__rows = np.array(A.row[:] + 1, dtype='i')
        self.__cols = np.array(A.col[:] + 1, dtype='i')

        if self.is_complex:
            self.__data = (self.__scal_type * nz)()
            for idx in range(0, nz):
                self.__data[idx].r = A.data.real[idx]
                self.__data[idx].i = A.data.imag[idx]
            self.id.a = cast(self.__data, POINTER(self.__scal_type))
        else:
            self.__data = A.data
            self.id.a = self.__data.ctypes.data_as(POINTER(self.__scal_type))


        if self.id.icntl[4] == 0:

            # Distributed input
            if self.id.icntl[17] == 3:

                self.id.n = c_int(n)
                self.id.nz_loc = c_int(nz)

                self.id.irn_loc = self.__rows.ctypes.data_as(POINTER(c_int))
                self.id.jcn_loc = self.__cols.ctypes.data_as(POINTER(c_int))

                if self.is_complex:
                    self.id.a_loc = cast(self.__data, POINTER(self.__scal_type))
                else:
                    self.id.a_loc = self.__data.ctypes.data_as(POINTER(self.__scal_type))

            # Centralized input
            elif self.id.icntl[17] == 0:
                self.id.n = c_int(n)
                self.id.nz = c_int(nz)

                self.id.irn = self.__rows.ctypes.data_as(POINTER(c_int))
                self.id.jcn = self.__cols.ctypes.data_as(POINTER(c_int))

                if self.is_complex:
                    self.id.a = cast(self.__data, POINTER(self.__scal_type))
                else:
                    self.id.a = self.__data.ctypes.data_as(POINTER(self.__scal_type))

            else:
                raise AttributeError("Only ICNTL[18] = 0 or 3 supported")

        else: # Elemental distribution

            raise AttributeError("Only ICNTL[5] = 0 supported")

    def set_RHS(self, rhs):
        """
        Assume rhs can be cast into a numpy.matrix
        rhs shape is n x n_rhs
        """
        self.__update_cntls()
        if np.int(self.id.icntl[19]) != 0:
            raise NotImplementedError("RHS must be dense (see icntl(20))")

        rhs = np.matrix(rhs, dtype=self.nptype)
        n = rhs.shape[0]
        n_rhs = rhs.shape[1]
        # If the rhs was an array, assume it was just a vector
        if n == 1 and n_rhs > 1:
            rhs = rhs.getT()
            n = rhs.shape[0]
            n_rhs = rhs.shape[1]

        self.id.nrhs = c_int(n_rhs)

        if n_rhs > 1:
            self.id.lrhs = c_int(n)

        if self.is_complex:
            self.__rhs = (self.__scal_type * (n * n_rhs))()
            for i in range(0, n):
                for j in range(0, n_rhs):
                    idx = j * n + i
                    self.__rhs[idx].r = rhs.real[i,j]
                    self.__rhs[idx].i = rhs.imag[i,j]
            self.id.rhs = cast(self.__rhs, POINTER(self.__scal_type))
        else:
            # Send column major array (fortran style)
            self.__rhs = np.array(rhs.flatten('F'), dtype=self.nptype)
            self.id.rhs = self.__rhs.ctypes.data_as(POINTER(self.__scal_type))

    def set_schur_listvar(self, listvar):
        """
        Set the list of indices of the Schur matrix (centalized mod)
        Nb: use python numbering in the list
        """
        self.__update_cntls()
        self.id.icntl[18] = 1
        self.__listvar = np.array(listvar, dtype='i') + 1 # For fortran numbering
        size_schur = len(self.__listvar)
        self.id.size_schur = size_schur
        self.id.listvar_schur = self.__listvar.ctypes.data_as(POINTER(c_int))

        self.__schur = np.zeros((size_schur ** 2,), dtype=self.nptype)
        self.id.schur = self.__schur.ctypes.data_as(POINTER(self.__scal_type))

    def get_schur(self):
        """
        Get the schur as a matrix
        """
        size_schur = self.id.size_schur
        return np.array(self.id.schur[0:size_schur**2]).reshape(size_schur, size_schur)

    def schur_forward(self, lredrhs=None):
        """
        Get f = b_2 - A_2,1 x A_1,1^-1 x b_1
        f is the reduced the right-hand side on the Schur
        """
        self.ICNTL[26] = 1
        self.__schur_f = np.zeros((self.id.size_schur,), dtype=self.nptype)
        self.id.redrhs = self.__schur_f.ctypes.data_as(POINTER(self.__scal_type))

        if self.id.nrhs > 1:
            self.id.lredrhs = lredrhs

        self.drive(3)
        return self.__schur_f

    def schur_backward(self, y, lredrhs=None):
        """
        Expand the Schur local solution on the complete solution variables.
        y is the solution corresponding to the schur variables
        """
        self.ICNTL[26] = 2
        self.id.redrhs = y.ctypes.data_as(POINTER(self.__scal_type))
        if self.id.nrhs > 1:
            self.id.lredrhs = lredrhs
        self.drive(3)
        return self.get_solution()

    def get_solution(self):
        """
        Get solution as a numpy array
        """
        n = int(self.id.n)
        n_rhs = int(self.id.nrhs)

        if self.is_complex:
            sol = np.zeros((n,n_rhs,), dtype=self.nptype)
            for i in range(0, n):
                for j in range(0, n_rhs):
                    idx = j * n + i
                    sol.real[i,j] = self.id.rhs[idx].r
                    sol.imag[i,j] = self.id.rhs[idx].i
            return np.array(sol)

        else:
            if self.id.nrhs == 1:
                return np.array(self.id.rhs[0:n], dtype=self.nptype)
            else:
                return np.array(self.id.rhs[0:n*self.id.nrhs], dtype=self.nptype).reshape((self.id.nrhs,n)).T
