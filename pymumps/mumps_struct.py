#
# Python wrapper for mumps
# 09/03/20
# Tested on mumps version 5.2.1
# Author: Gilles Marait
#

from ctypes import *

class Mumps_struct():

    MUMPS_VERSION_MAX_LEN = 30

    ###
    # Mumps scalar data types

    class cmumps_complex(Structure):
        _fields_ = [("r", c_float),
                    ("i", c_float)]

    dmumps_complex = c_double
    smumps_complex = c_float

    class zmumps_complex(Structure):
        _fields_ = [("r", c_double),
                    ("i", c_double)]

    # Create the mumps structures

    class cmumps_struct(Structure):
        pass

    class dmumps_struct(Structure):
        pass

    class smumps_struct(Structure):
        pass

    class zmumps_struct(Structure):
        pass

    #
    ###

    def __init__(self, precision):
        precs = ['c', 'd', 's', 'z']
        if precision not in precs:
            raise ValueError("Precision given is " + str(precision) + ". Not in " + str(precs))

        self.precision = precision

    def get_struct_type(self):
        try:
            Mumps_struct.cmumps_struct._fields_ = self.__get_fields('c')
            Mumps_struct.dmumps_struct._fields_ = self.__get_fields('d')
            Mumps_struct.smumps_struct._fields_ = self.__get_fields('s')
            Mumps_struct.zmumps_struct._fields_ = self.__get_fields('z')
        except AttributeError:
            # Ignore error if the values are already set
            pass
        struct_dict = {'c' : Mumps_struct.cmumps_struct,
                       'd' : Mumps_struct.dmumps_struct,
                       's' : Mumps_struct.smumps_struct,
                       'z' : Mumps_struct.zmumps_struct}
        return struct_dict[self.precision]

    def get_scal_type(self):
        scal_dict = {'c' : Mumps_struct.cmumps_complex,
                     'd' : Mumps_struct.dmumps_complex,
                     's' : Mumps_struct.smumps_complex,
                     'z' : Mumps_struct.zmumps_complex}
        return scal_dict[self.precision]

    def get_real_type(self):
        real_dict = {'c' : c_float,
                     'd' : c_double,
                     's' : c_float,
                     'z' : c_double}
        return real_dict[self.precision]

    def get_numpy_type(self):
        np_dict = {'c' : 'c8',
                   'd' : 'f8',
                   's' : 'f4',
                   'z' : 'c16'}

        return np_dict[self.precision]

    def __get_fields(self, precision, int64=False):
        """
        Create the structure fields depending on the scalar type
        """
        scal = self.get_scal_type()
        real = self.get_real_type()
        mumps_int = c_int64 if int64 else c_int

        return  [("sym", mumps_int),
                 ("par", mumps_int),
                 ("job", mumps_int),
                 ("comm_fortran", mumps_int),
                 ("icntl", mumps_int * 60),
                 ("keep", mumps_int * 500),
                 ("cntl", real * 15),
                 ("dkeep", real * 230),
                 ("keep8", c_int64 * 150),
                 ("n", mumps_int),
                 ("nz_alloc", mumps_int),
                 ("nz", mumps_int),
                 ("nnz", c_int64),
                 ("irn", POINTER(mumps_int)),
                 ("jcn", POINTER(mumps_int)),
                 ("a", POINTER(scal)),
                 ("nz_loc", mumps_int),
                 ("nnz_loc", c_int64),
                 ("irn_loc", POINTER(mumps_int)),
                 ("jcn_loc", POINTER(mumps_int)),
                 ("a_loc", POINTER(scal)),
                 ("nelt", mumps_int),
                 ("eltptr", POINTER(mumps_int)),
                 ("eltvar", POINTER(mumps_int)),
                 ("a_elt", POINTER(scal)),
                 ("perm_in", POINTER(mumps_int)),
                 ("sym_perm", POINTER(mumps_int)),
                 ("uns_perm", POINTER(mumps_int)),
                 ("colsca", POINTER(real)),
                 ("rowsca", POINTER(real)),
                 ("colsca_from_mumps", mumps_int),
                 ("rowsca_from_mumps", mumps_int),
                 ("rhs",        POINTER(scal)),
                 ("redrhs",     POINTER(scal)),
                 ("rhs_sparse", POINTER(scal)),
                 ("sol_loc",    POINTER(scal)),
                 ("rhs_loc",    POINTER(scal)),
                 ("irhs_sparse", POINTER(mumps_int)),
                 ("irhs_ptr",    POINTER(mumps_int)),
                 ("isol_loc",    POINTER(mumps_int)),
                 ("irhs_loc",    POINTER(mumps_int)),
                 ("nrhs",       mumps_int),
                 ("lrhs",       mumps_int),
                 ("lredrhs",    mumps_int),
                 ("nz_rhs",     mumps_int),
                 ("lsol_loc",   mumps_int),
                 ("nloc_rhs",   mumps_int),
                 ("lrhs_loc",   mumps_int),                 
                 ("schur_mloc", mumps_int),
                 ("schur_nloc", mumps_int),
                 ("schur_lld",  mumps_int),
                 ("mblock", mumps_int),
                 ("nblock", mumps_int),
                 ("nprow", mumps_int),
                 ("npcol", mumps_int),
                 ("info", mumps_int * 80),
                 ("infog", mumps_int * 80),
                 ("rinfo", real * 40),
                 ("rinfog", real * 40),
                 ("deficiency", mumps_int),
                 ("pivnul_list", POINTER(mumps_int)),
                 ("mapping", POINTER(mumps_int)),
                 ("size_schur", mumps_int),
                 ("listvar_schur", POINTER(mumps_int)),
                 ("schur", POINTER(scal)),
                 ("instance_number", mumps_int),
                 ("wk_user", POINTER(scal)),
                 ("version_number", c_char * (Mumps_struct.MUMPS_VERSION_MAX_LEN + 2)),
                 ("ooc_tmpdir", c_char * 256),
                 ("ooc_prefix", c_char * 64),
                 ("write_problem", c_char * 256),
                 ("lwk_user", mumps_int),
                 ("save_dir", c_char * 256),
                 ("save_prefix", c_char * 256),
                 ("metis_options", mumps_int * 40)]
